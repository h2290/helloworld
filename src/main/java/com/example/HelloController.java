package com.example;

import java.util.Map;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("hello")
public class HelloController {

    @GetMapping(path = "say")
    public String sayHello() {
        return "Hello, world!";
    }

    @GetMapping(path = "say/{name}")
    public String sayHelloToCaller(@PathVariable String name) {
        return "Hello, " + name + "!";
    }

    @GetMapping(path = "say/ifran")
    public Map<String, String> sayHelloToCaller() {
        return Map.of("message", "Hello, moron!");
    }
}
